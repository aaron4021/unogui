from tkinter import *
from PIL import ImageTk, Image
import classcard
from classcard import Card,Player
import random

class MENU:
	def __init__(self,master):
		self.number = 0
		self.master = master
		master.geometry = ("1920x1080")
		self.button_1 = Button(master, text = "Start", command = self.playerget, width = 100)
		self.bar = Entry(master, textvariable = self.number, width = 200)
		self.bar.grid(row = 2, columnspan = 10)
		self.button_1.grid(row = 5,columnspan = 10)

	def playerget(self):
		global number
		number = self.bar.get()
		self.master.destroy()
#class unogui
class UNOGUI:
	def __init__(self,master,number):
		self.number = number
		self.master = master
		print("b")
		self.initializePlayer()

	def initializePlayer(self):
		self.player = []
		for i in range (int(number)):
			self.player.append(Player(i+1))
		self.initializeCardstack()
	def initializeCardstack(self):
		self.color = ["RED","GREEN",'BLUE', "YELLOW"]
		self.wild = ["+4","WLD"]
		self.abilities = ["REVERSE", "SKIP", "+2"]
		self.cardstack = []
		for i in range (4):
			for n in range (10):
				if n != 0:
					self.cardstack.append(Card(self.color[i],str(n)))
				self.cardstack.append(Card(self.color[i],str(n)))
			for m in range (3):
				self.cardstack.append(Card(self.color[i],self.abilities[m]))
				self.cardstack.append(Card(self.color[i],self.abilities[m]))
		for i in range (len(self.abilities)-1):
			for m in range (4):
				self.cardstack.append(Card("BLACK",self.wild[i]))
		self.initializePlayerCard()
	def initializePlayerCard(self):
		print("j")
		for i in range (len(self.player)):
			for m in range (7):
				temp = random.randint(0,len(self.cardstack)-1)
				self.player[i].addcard(self.cardstack[temp])
				self.cardstack.pop(temp)
		self.gamestart()
	def gamestart(self):
		print("2")
		
		temp = self.cardstack[random.randint(0,len(self.cardstack)-1)]
		if temp.getcolor() == "BLACK" or temp.getnumber() in self.abilities:
			return
		else:
			self.pile = temp
			image = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.pile.getcolor().lower()+'_'+self.pile.getnumber().lower()+'.png'))
			
			self.cardlabel = Label(self.master, text = self.pile, image = image)
			self.cardlabel.image = image
			self.cardlabel.grid(row = 4, column = 3)
		self.win = False
		self.marker = False
		self.i = 0
		self.skip = False
		self.reverse = False
		self.plus4 = False
		self.plus2 = False
		self.flow = 1
		self.a()
	def a(self):
		img = ImageTk.PhotoImage(Image.open(r"uno_assets_2d/uno_assets_2d/PNGs/small/card_back.png"))
		if self.plus2:
			self.plus2 = False
			for p in range (2):
				tempp = self.cardstack.pop(random.randint(0,len(self.cardstack)-1))
				self.player[self.i].addcard(tempp)
			for i in range (len(self.cardbuttonarray)):
				self.cardbuttonarray[i].destroy()

			self.i = (self.i+self.flow)%len(self.player)
		if self.plus4:
			self.plus4 = False
			for t in range (4):
				tempp = self.cardstack.pop(random.randint(0,len(self.cardstack)-1))
				self.player[self.i].addcard(tempp)
			for i in range (len(self.cardbuttonarray)):
				self.cardbuttonarray[i].destroy()
			print(self.i)
			self.i = (self.i+self.flow)%len(self.player)
			print(self.i)
		
		image = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.pile.getcolor().lower()+'_'+self.pile.getnumber().lower()+'.png'))
		self.cardlabel.image = image
		self.cardlabel["image"] = image
		self.playerturn = Label(self.master,text = "Player " + str(self.i) + " turn")
		self.playerturn.grid(row = 1)
		self.cardpile = self.player[self.i].getcard()
		

		self.cardbuttonarray = []
		self.draw = Button(self.master, text= "Draw Cards",image = img, command = lambda: self.drawmechanic(self.player[self.i]))
		self.draw.image = img
		self.draw.grid(row = 8, column = 0)
		for i in range (len(self.cardpile)):
			photo = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.cardpile[i].getcolor().lower()+'_'+self.cardpile[i].getnumber().lower()+'.png'))
			self.cardbutton = Button(self.master, text = self.cardpile[i], image = photo)
			self.cardbutton.image = photo
			self.cardbuttonarray.append(self.cardbutton)
			self.cardbuttonarray[i]["command"] = lambda idx = i: self.gamemechanic(self.cardpile[idx], self.player[self.i], idx)
			self.cardbuttonarray[i].grid(row = 9, column = i)
		for i in range (len(self.cardpile)):
			print(self.cardpile[i])
		self.drawbutton = Button(self.master, text = "Draw Cards", command = self.drawmechanic)
	def b(self,player):
		if len(player.getcard()) == 0:
			win = True
			self.winscreen = Label(self.master, text = "Player"+str(self.i)+"wins")
			self.winscreen.grid(row = 11)
		else:
			self.a()
	def gamemechanic(self, cardnow, player, number):
		print(number)
		print("Button pressed")

		for i in range (len(self.cardbuttonarray)):
			self.cardbuttonarray[i].destroy()
		if cardnow.getcolor() == self.pile.getcolor() or cardnow.getcolor() == "BLACK" or cardnow.getnumber() == self.pile.getnumber():
			self.pile = cardnow
			image = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.pile.getcolor().lower()+'_'+self.pile.getnumber().lower()+'.png'))
			self.cardlabel.image = image
			self.cardlabel["image"] = image
			player.rcard(number)
			self.blackwldarray = []
			self.blackplusfourarray = []
			if self.pile.getnumber() == "WLD":
				for i in range (len(self.cardbuttonarray)):
					self.cardbuttonarray[i].destroy()
				for i in range (4):
					buttonimage = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.color[i]+'_'+'1'+'.png'))
					self.blackwld = Button(self.master, image = buttonimage)
					self.blackwld.image = buttonimage
					self.blackwldarray.append(self.blackwld)
					self.blackwldarray[i]["command"] = lambda idx = i : self.wildcardfunction(self.color[idx])
					self.blackwldarray[i].grid(row = 5, column = i)


			if self.pile.getnumber() == "+4":
				self.plus4 = True
				for i in range (len(self.cardbuttonarray)):
					self.cardbuttonarray[i].destroy()
				for i in range (4):
					buttonimage = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.color[i]+'_'+'1'+'.png'))
					self.blackplusfour = Button(self.master, image = buttonimage)
					self.blackplusfour.image = buttonimage
					self.blackplusfourarray.append(self.blackplusfour)
					self.blackplusfourarray[i]["command"] = lambda idx = i : self.plus4function(self.color[idx])
					self.blackplusfourarray[i].grid(row = 5, column = i)
			if self.pile.getnumber() == "SKIP":
				self.skip = True
			if self.pile.getnumber() == "REVERSE":
				self.flow = self.flow*-1
				for i in range (len(self.cardbuttonarray)):
					self.cardbuttonarray[i].destroy()
			if self.pile.getnumber() == "+2":
				self.plus2 = True
		if self.skip:
			self.skip = False
			self.i = (self.i+self.flow)%len(self.player)
		
			
		

		self.i = (self.i +self.flow)%len(self.player)
		#self.a()
		self.b(self.player[self.i])
	def drawmechanic(self,player):

		tempn = self.cardstack.pop(random.randint(0, len(self.cardstack)-1))
		if tempn.getcolor() == self.pile.getcolor() or tempn.getcolor() == "BLACK" or tempn.getnumber()==self.pile.getnumber():
			self.pile = tempn
			image = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.pile.getcolor().lower()+'_'+self.pile.getnumber().lower()+'.png'))
			self.cardlabel.image = image
			self.cardlabel["image"] = image
			self.blackwldarray = []
			self.blackplusfourarray = []
			if self.pile.getnumber() == "WLD":
				for i in range (len(self.cardbuttonarray)):
					self.cardbuttonarray[i].destroy()
				for i in range (4):
					buttonimage = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.color[i]+'_'+'1'+'.png'))
					self.blackwld = Button(self.master, image = buttonimage)
					self.blackwld.image = buttonimage
					self.blackwldarray.append(self.blackwld)
					self.blackwldarray[i]["command"] = lambda idx = i : self.wildcardfunction(self.color[idx])
					self.blackwldarray[i].grid(row = 5, column = i)


			if self.pile.getnumber() == "+4":
				self.plus4 = True
				for i in range (len(self.cardbuttonarray)):
					self.cardbuttonarray[i].destroy()
				for i in range (4):
					buttonimage = ImageTk.PhotoImage(Image.open(r'uno_assets_2d/uno_assets_2d/PNGs/small/'+self.color[i]+'_'+'1'+'.png'))
					self.blackplusfour = Button(self.master, image = buttonimage)
					self.blackplusfour.image = buttonimage
					self.blackplusfourarray.append(self.blackplusfour)
					self.blackplusfourarray[i]["command"] = lambda idx = i : self.plus4function(self.color[idx])
					self.blackplusfourarray[i].grid(row = 5, column = i)
			if self.pile.getnumber() == "SKIP":
				self.skip = True
			if self.pile.getnumber() == "REVERSE":
				self.flow = self.flow*-1
				for i in range (len(self.cardbuttonarray)):
					self.cardbuttonarray[i].destroy()
			if self.pile.getnumber() == "+2":
				self.plus2 = True
			if self.skip:
				self.skip = False
				self.i = (self.i+self.flow)%len(self.player)
			
			
		

			self.i = (self.i +self.flow)%len(self.player)
			self.a()

		else:
			self.player[self.i].addcard(tempn)
			self.cardpile = self.player[self.i].getcard()
			for i in range (len(self.cardpile)):
				print(self.cardpile)
			for i in range (len(self.cardbuttonarray)):
				self.cardbuttonarray[i].destroy()
			self.i = (self.i+self.flow)%len(self.player)
			self.a()




	def wildcardfunction(self,color):
		for i in range (len(self.blackwldarray)):
			self.blackwldarray[i].destroy()
		
		self.pile = Card(color,'1')
		self.cardlabel["text"] = self.pile
		


	def plus4function(self,color):
		for i in range (len(self.blackplusfourarray)):
			self.blackplusfourarray[i].destroy()
	
		self.pile = Card(color,'1')
		self.cardlabel["text"] = self.pile
		
		

window = Tk()
menugui = MENU(window)
number = 0
window.mainloop()

window = Tk()
print(number)
unogui = UNOGUI(window,number)
window.mainloop()
		